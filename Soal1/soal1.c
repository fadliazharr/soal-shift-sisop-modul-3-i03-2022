#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void createdirectory (char *dest) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"mkdir", "-p", dest, NULL};
execv("/usr/bin/mkdir", argv);
} else {
((wait(&status)) > 0);
}
}

void zipextract(char *dest, char *src) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
execv("/usr/bin/unzip", argv);
} else {
((wait(&status)) > 0);
}
}

int main() {

createdirectory ("/home/rahel/soal1/music/");
zipextract ("/home/rahel/soal1/music/", "/home/rahel/soal1/music.zip");
createdirectory ("/home/rahel/shift2/music");

createdirectory ("/home/rahel/soal1/quote/");
zipextract ("/home/rahel/soal1/quote/", "/home/rahel/soal1/quote.zip");
createdirectory ("/home/rahel/soal1/quote");

return 0;
}

