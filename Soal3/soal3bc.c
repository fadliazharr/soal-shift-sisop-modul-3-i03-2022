#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>

int FileEx(const char *filename) {

    struct stat buff;
    int exist = stat(filename, &buff);
    if (exist == 0)
        return 1;
    else 
        return 0;
}

void *move(void *filename) {
    char cwd[PATH_MAX];
    char direname[500];
    char hide[500], hidename[500];
    char file[500], exfile[500];
    int i;
    strcpy(exfile, filename);
    strcpy(hidename, filename);
    char *nama = strrchr(hidename, '/');
    strcpy(hide, nama);

    if (hide[1] == '.') {
        strcpy(direname, "Hidden");
    }
   
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
   
    } else {
        strcpy(direname, "Unknown");
    }

    int exist = fileex(exfile);
    if (exist)
        mkdir(direname, 0755);

    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char filenamee[200];
        strcpy(filenamee, cwd);
        strcat(filenamee, "/");
        strcat(filenamee, direname);
        strcat(filenamee, nama);
        rename(filename, filenamee);
    }
}

void FileRec(char *basePath) {
    char path[1001];
    struct dirent *dr;
    struct stat buff;
    DIR *direc = opendir(basePath);
    int n = 0;

    if (!direc)
        return;

    while ((dr = readdir(direc)) != NULL) {
        if (strcmp(dr -> d_name, ".") != 0 && strcmp(dr -> d_name, "..") != 0)   {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dr -> d_name);

            if (stat(path, &buff) == 0 && S_ISREG(buff.st_mode))  {
                pthread_t thr;
                int err = pthread_create(&thr, NULL, move, (void *)path);
                pthread_join(thr, NULL);
            }

            FileRec(path);
        }
    }
    closedir(direc);
}

int main(int argc, char *argv[]) {
    char cwd[PATH_MAX];
    if (strcmp(argv[1], "-f") == 0) {
        pthread_t thr;
        int i;
        
        for (i = 2; i < argc; i++) {
            char prnt[1000];
            int exist = FileEx(argv[i]);
            if (exist) {
                sprintf(prnt, "File %d : Category success", i - 1);
            } else{
                sprintf(prnt, "File %d : Failed to category ", i - 1);
            }
            printf("%s\n", prnt);
            int et = pthread_create(&thr, NULL, move, (void *)argv[i]);
        }

        pthread_join(thr, NULL);
    } else {
    
        if (strcmp(argv[1], "*") == 0) {
            if (getcwd(cwd, sizeof(cwd)) != NULL)  {
                 FileRec(cwd);
            }
        } else if (strcmp(argv[1], "-d") == 0) {          
            FileRec(argv[2]);
            struct stat buff;
            int et = stat(argv[2], &buff);
            if (et == -1) {
                printf("not save \n");
            } else {
                printf("Save success \n");
            }
        }
    }
}
