#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/wait.h>
#include <string.h>

void createdire (char *dest) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0 ) {
        char *argv[] = {"mkdir", "-p", dest, NULL};
        execv ("usr/bin/mkdir", argv);
    }else {
        ((wait(&status)) > 0);
    }
}

void zipextract (char *dest, char *src) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
        execv ("usr/bin/unzip", argv);
    }else {
        ((wait(&status)) > 0 );
    }
}
int main () {
    createdire ("/home/erlangga2/modul3/hartakarun/");
    zipextract ("/home/erlangga2/modul3/hartakarun.zip" , "/home/erlangga2/modul3/hartakarun");

    return 0;

}
