# soal-shift-sisop-modul-3-I03-2022

## Kelompok I03
Erlangga Wahyu Utomo - 5025201118
Muhammad Fadli Azhar - 5025201157
Rahel Cecilia Purba  - 5025201155

## 1A
Pada soal 1a praktikan diminta untuk mendownload dua file zip yang lalu akan diunzip file tersebut di dua folder yang terpisah dengan nama quote untuk file quote.zip dan music untuk file music.zip
• Pertama-tama kita membuat directory sebagai destinasi folder yang ingin di unzip
• Lalu kemudian kita fungsi zip extract menggunakan unzip
• Setelah itu kita membuat folder yang dimana sebagai tempatnya file file yang sudah di ekstrak.
• Kemudian masing-masing file dibuatkan folder untuk menyimpan sesuai dengan jenis file nya.

![hasil1a.1](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-3-i03-2022/-/blob/main/Img/1650202988866.png)
![hasil1a.2](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-3-i03-2022/-/blob/main/Img/1650203018420.png)

```
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void createdirectory (char *dest) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"mkdir", "-p", dest, NULL};
execv("/usr/bin/mkdir", argv);
} else {
((wait(&status)) > 0);
}
}

void zipextract(char *dest, char *src) {
int status;
pid_t child_id = fork();
if (child_id == 0) {
char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
execv("/usr/bin/unzip", argv);
} else {
((wait(&status)) > 0);
}
}

int main() {

createdirectory ("/home/rahel/soal1/music/");
zipextract ("/home/rahel/soal1/music/", "/home/rahel/soal1/music.zip");
createdirectory ("/home/rahel/shift2/music");

createdirectory ("/home/rahel/soal1/quote/");
zipextract ("/home/rahel/soal1/quote/", "/home/rahel/soal1/quote.zip");
createdirectory ("/home/rahel/soal1/quote");

return 0;
}
```

## 2A
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
-Username unique (tidak boleh ada user yang memiliki username yang sama)
-Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

Praktikan diminta untuk membuat dua pilihan register dan login saat client telah terhubung ke server, client akan diminta memasukan input id dan password. Data input akan disimpan ke file users.txt dengan format username:password, Jika klien memilih untuk masuk server akan juga meminta id dan password server kemudian akan mencari data dalam file users.txt yang sama dengan input. Jika data ditemukan maka klien akan masuk dan dapat menggunakan perintah yang ada di sistem.

## 3A
a.	Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif Praktikan diminta untuk mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Praktikan juga harus mengkategorikan seluruh file pada working directory secara rekursif

![hasil3a](https://gitlab.com/fadliazharr/soal-shift-sisop-modul-3-i03-2022/-/blob/main/Img/3a.png)
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/wait.h>
#include <string.h>

void createdire (char *dest) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0 ) {
        char *argv[] = {"mkdir", "-p", dest, NULL};
        execv ("usr/bin/mkdir", argv);
    }else {
        ((wait(&status)) > 0);
    }
}

void zipextract (char *dest, char *src) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"unzip", "-j", src, "-d", dest, NULL};
        execv ("usr/bin/unzip", argv);
    }else {
        ((wait(&status)) > 0 );
    }
}
int main () {
    createdire ("/home/erlangga2/modul3/hartakarun/");
    zipextract ("/home/erlangga2/modul3/hartakarun.zip" , "/home/erlangga2/modul3/hartakarun");

    return 0;

}
```
b.	Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”. 
 Fileex untuk cek eksistensi file lalu pada function move memindahkan ke kategori masing". untuk Hidden itu untuk file yg mempunyai awalan '.', jika tidak makan ke file Unknown
if (get cwd) untuk pembuatan folder/directory yg sesuai nama file. Untuk FileRec yaitu untuk list kategori file secara berulang/rekursif. jika run berhasil, akan muncul Category Success dan file telah terorganisir sesuai nama.
c.	Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

## Source code 3bc
```
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>

int FileEx(const char *filename) {

    struct stat buff;
    int exist = stat(filename, &buff);
    if (exist == 0)
        return 1;
    else 
        return 0;
}

void *move(void *filename) {
    char cwd[PATH_MAX];
    char direname[500];
    char hide[500], hidename[500];
    char file[500], exfile[500];
    int i;
    strcpy(exfile, filename);
    strcpy(hidename, filename);
    char *nama = strrchr(hidename, '/');
    strcpy(hide, nama);

    if (hide[1] == '.') {
        strcpy(direname, "Hidden");
    }
   
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
   
    } else {
        strcpy(direname, "Unknown");
    }

    int exist = fileex(exfile);
    if (exist)
        mkdir(direname, 0755);

    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char filenamee[200];
        strcpy(filenamee, cwd);
        strcat(filenamee, "/");
        strcat(filenamee, direname);
        strcat(filenamee, nama);
        rename(filename, filenamee);
    }
}

void FileRec(char *basePath) {
    char path[1001];
    struct dirent *dr;
    struct stat buff;
    DIR *direc = opendir(basePath);
    int n = 0;

    if (!direc)
        return;

    while ((dr = readdir(direc)) != NULL) {
        if (strcmp(dr -> d_name, ".") != 0 && strcmp(dr -> d_name, "..") != 0)   {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dr -> d_name);

            if (stat(path, &buff) == 0 && S_ISREG(buff.st_mode))  {
                pthread_t thr;
                int err = pthread_create(&thr, NULL, move, (void *)path);
                pthread_join(thr, NULL);
            }

            FileRec(path);
        }
    }
    closedir(direc);
}

int main(int argc, char *argv[]) {
    char cwd[PATH_MAX];
    if (strcmp(argv[1], "-f") == 0) {
        pthread_t thr;
        int i;
        
        for (i = 2; i < argc; i++) {
            char prnt[1000];
            int exist = FileEx(argv[i]);
            if (exist) {
                sprintf(prnt, "File %d : Category success", i - 1);
            } else{
                sprintf(prnt, "File %d : Failed to category ", i - 1);
            }
            printf("%s\n", prnt);
            int et = pthread_create(&thr, NULL, move, (void *)argv[i]);
        }

        pthread_join(thr, NULL);
    } else {
    
        if (strcmp(argv[1], "*") == 0) {
            if (getcwd(cwd, sizeof(cwd)) != NULL)  {
                 FileRec(cwd);
            }
        } else if (strcmp(argv[1], "-d") == 0) {          
            FileRec(argv[2]);
            struct stat buff;
            int et = stat(argv[2], &buff);
            if (et == -1) {
                printf("not save \n");
            } else {
                printf("Save success \n");
            }
        }
    }
}
```
